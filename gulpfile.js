const gulp = require('gulp');
const elixir = require('laravel-elixir');

require('laravel-elixir-vue');

const basset = require('laravel-basset');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

// elixir(mix => {
// 		mix.sass('./resources/assets/vue/sass/app.scss')
// 			 .webpack('./resources/assets/vue/app.js');
// });

/*
 |--------------------------------------------------------------------------
 | Basset
 |--------------------------------------------------------------------------
 */

basset.loadFile('./collections.json');

gulp.task('default', ['build', 'basset:watch'])
